package com.fisdom;

import com.fisdom.Exceptions.NullNameException;

public class Education {
    // Поля класса

    private Specialty specialty;
    private Degree degree;

    /** Конструктор класса Education
     * @param specialty - специальность
     * @param degree - степень образования
     * */
    public Education(Specialty specialty, Degree degree){
        if(specialty == null && degree == null){
            throw new NullNameException("com.fisdom.Education name can't be null!");
        }
        this.specialty = specialty;
        this.degree = degree;
    }

    // Геттеры

    public Specialty getSpecialty() {
        return specialty;
    }

    public Degree getDegree() {
        return degree;
    }

    // Методы класса Education

    /** Обновить образование. Обновление иожет производиться только в сторону увеличения
     * @param education - повышенное образование
     * @return boolean - успешность обновления образования
     * */
    public boolean updateEducation(Education education){
        boolean canBeUpdated = education != null && education.degree.compare(this.degree) > 0;
        if(canBeUpdated){
            this.specialty = education.specialty;
            this.degree = education.degree;
        }
        return canBeUpdated;
    }
}
