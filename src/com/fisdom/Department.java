package com.fisdom;

import com.fisdom.Exceptions.NullInDepartmentException;
import com.fisdom.Exceptions.NullNameException;
import com.fisdom.worker.Worker;

import java.util.HashSet;

public class Department {
    // Поля класса
    private String departName;
    private HashSet<Worker> workers = new HashSet<Worker>();

    /** Конструктор класса Department
     * @param departName - название отдела
     * */
    public Department(String departName){
        if(this.departName == null){
            throw new NullNameException("com.fisdom.Department name can't be null!");
        }
        this.departName = departName;
    }

    // Методы класса Department

    /** Добавить работника в отдел
     * @param worker - работник, который будет добавлен в отдел
     * @return boolean - успешность добавления работника
     * */
    public boolean addWorker(Worker worker){
        if(worker == null){
            throw new NullInDepartmentException("Worker can't be null!");
        }
        return this.workers.add(worker);
    }

    /** Убрать работника из отдела
     * @param worker - работник, который будет убран из отдела
     * */
    public void removeWorker(Worker worker){
        if(worker == null){
            throw new NullInDepartmentException("Worker can't be null!");
        }
        this.workers.remove(worker);
    }

    // Геттеры

    public String getDepartName() {
        return departName;
    }

    public HashSet<Worker> getWorkers() {
        return workers;
    }
}
