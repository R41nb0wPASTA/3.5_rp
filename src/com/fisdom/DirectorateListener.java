package com.fisdom;

import com.fisdom.worker.Worker;

public interface DirectorateListener {
    public void updateInfo(Worker worker, String newInfo);
}
