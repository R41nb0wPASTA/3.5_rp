package com.fisdom;

public enum Specialty {
    // Перечисление
    FIRST_PROF(0, "Первая профессия"),
    SECOND_PROF(1, "Вторая профессия");

    // Поля перечисления
    private long id;
    private String profName;

    /** Конструктор перечисления Degree
     * @param id - id профессии
     * @param profName - название профессии
     * */
    Specialty(long id, String profName){
        this.id = id;
        this.profName = profName;
    }

    // Геттеры

    public String getProfName() {
        return profName;
    }
}
