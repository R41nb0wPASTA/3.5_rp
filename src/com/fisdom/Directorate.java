package com.fisdom;

import com.fisdom.Exceptions.DirectorateException;
import com.fisdom.worker.Worker;

import java.util.ArrayList;
import java.util.HashMap;

public class Directorate implements DirectorateListener {
    // Поля класса

    private HashMap<Worker, ArrayList<String>> report;

    /** Конструктор класса Department **/
    public Directorate() {
        report = new HashMap<>();
    }

    // Методы класса Directorate

    /** Сформировать отчёт о сотруднике
     * @param worker - сотрудник, о котором будет сформирован отчёт
     * @return String - отчёт о сотруднике
     * */
    public String createReport(Worker worker){
        if(worker == null || report.get(worker) == null){
            throw new DirectorateException("Worker is not found in directorate or is null!");
        }
        return String.join("\n", report.get(worker));
    }

    /** Обновить данные о сотруднике
     * @param worker - сотрудник, о котором будут обновлены данные
     * @param newInfo - информация для обновления
     * */
    public void updateInfo(Worker worker, String newInfo) {
        if(worker == null || newInfo == null){
            throw new DirectorateException("Worker and newInfo can't be nulls!");
        }

        if (this.report.get(worker) != null)
            this.report.get(worker).add(newInfo);
        else {
            ArrayList<String> temp = new ArrayList<>();
            temp.add(newInfo);
            this.report.put(worker, temp);
        }
    }

    // Геттеры

    /** Геттер отчёта обо всех сотрудниках
     * @return HashMap - отчёт обо всех сотрудниках
     * */
    public HashMap<Worker, ArrayList<String>> getReport() {
        return report;
    }

}
