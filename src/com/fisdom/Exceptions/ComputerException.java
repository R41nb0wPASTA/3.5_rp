package com.fisdom.Exceptions;

public class ComputerException extends RuntimeException{
    public ComputerException(String message) {super(message);}
}
