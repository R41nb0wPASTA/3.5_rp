package com.fisdom.Exceptions;

public class NullInDepartmentException extends RuntimeException{
    public NullInDepartmentException(String message) {super(message);}
}
