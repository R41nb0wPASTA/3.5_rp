package com.fisdom.Exceptions;

public class NullNameException extends RuntimeException{
    public NullNameException(String message) {super(message);}
}
