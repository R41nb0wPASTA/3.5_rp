package com.fisdom;

public enum Degree {
    // Перечисление
    HIGH_SCHOOL(0, "Старшая школа"),
    COLLEGE(0, "Колледж"),
    ASSOCIATE(0, "Младший специалист"),
    BACHELOR(0, "Бакалавр"),
    MASTER(0, "Магистр"),
    DOCTOR(0, "Доктор");

    // Поля перечисления
    private long id;
    private String degreeName;

    /** Конструктор перечисления Degree
     * @param id - id степени
     * @param degreeName - название степени
     * */
    Degree(long id, String degreeName){
        this.id = id;
        this.degreeName = degreeName;
    }

    // Геттеры

    public String getDegreeName() {
        return degreeName;
    }

    // Методы перечисления Degree
    /** Сравнить два перечисления
     * @param toBeComparedWith - степень, с которой будет производиться сравнение
     * @return int - +1 - больше, 0 - равно, -1 - меньше
     * */
    public int compare(Degree toBeComparedWith) {
        return Long.compare(this.id, toBeComparedWith.id);
    }

}
