package com.fisdom.worker;

import com.fisdom.Computer;
import com.fisdom.Department;
import com.fisdom.Directorate;
import com.fisdom.Education;
import com.fisdom.Exceptions.WorkerException;

public class Worker {
    // Поля класса
    private Card card;
    private Education education;
    private Directorate directorate;
    private Department department;
    private Computer computer;

    private String workerName;

    /** Конструктор класса Worker
     * @param workerName - ФИО работника
     * @param department - отдел, в который работник будет назначен
     * @param education - образование работника
     * @param directorate - дирекция работника
     * */
    public Worker(String workerName, Department department, Education education, Directorate directorate) {
        this(workerName, education, directorate);
        if(!this.hire(department))
            throw new WorkerException("Department can't be null");
    }

    /** Конструктор класса Worker без назначения его в отдел
     * @param workerName - ФИО работника
     * @param education - образование работника
     * @param directorate - дирекция работника
     * */
    public Worker(String workerName, Education education, Directorate directorate) {
        String patternName = "(\\p{Upper}(\\p{Lower}+\\s?)){2,3}";

        if(workerName == null && education == null && directorate == null){
            throw new WorkerException("Name, education and directorate can't be null!");
        }
        else if(!workerName.matches(patternName)){
            throw new WorkerException("Name should follow the regular pattern!");
        }

        this.workerName = workerName;
        this.education = education;
        this.directorate = directorate;
        this.card = null;
    }

    // Геттеры

    public Card getCard() {
        return card;
    }

    public Education getEducation() {
        return education;
    }

    public Directorate getDirectorate() {
        return directorate;
    }

    public Department getDepartment() {
        return department;
    }

    public Computer getComputer() {
        return computer;
    }

    public String getWorkerName() {
        return workerName;
    }

    // Методы класса Worker

    /** Начать использование компьютера
     * @param computer - компьютер, котоый будет использовать работник
     * @return boolean - успешность начала использования
     * */
    public boolean setComputer(Computer computer){
        boolean isSet = computer != null && this.directorate != null;
        if(!isSet){
            throw new WorkerException("Computer can't  be null!");
        }
        this.computer = computer;

        return isSet;
    }

    /** Обновить образование. Обновление проводится только в сторону повышения
     * @param education - повышенное образование
     * */
    public void upgradeEducation(Education education){
        if(education == null || !this.education.updateEducation(education)){
            throw new WorkerException("Education can't be lower than the current one and null!");
        }
        directorate.updateInfo(this, this.workerName + " получил степень"+
                                                    this.education.getDegree().getDegreeName() +
                                                     " по " + this.education.getSpecialty().getProfName());
    }

    /** Нанять работнка в отдел
     * @param department - отдел, в который будет назначен работник
     * @return boolean - успешность нанимания
     * */
    public boolean hire(Department department){
        if(department == null){
            throw new WorkerException("Deoartment can't be null!");
        }

        boolean isHired = department != null && department.addWorker(this);
        if(isHired) {
            this.department = department;
            this.card = new Card();
            this.directorate.updateInfo(this, this.workerName + " принят в отдел " +
                                                             this.department.getDepartName());
        }

        return isHired;
    }

    /** Переместить работника из одного отдела в другой
     * @param department - отдел, в который будет переведён работник
     * @return boolean - успешность перевода
     * */
    public boolean moveToNewDepartment(Department department){
        if(department == null){
            throw new WorkerException("Department can't be null!");
        }

        boolean isMoved = department != null && !department.equals(this.department)
                && this.department != null;
        if(isMoved){
            this.department.removeWorker(this);
            this.department = department;

            if(this.computer != null)
                this.computer.finishUsage();
            this.computer = null;

            this.card = new Card();
            directorate.updateInfo(this,this.workerName + " переведен в отдел " + this.department.getDepartName());
        }
        return isMoved;
    }

    /** Уволить работника **/
    public void fire(){
        this.department.removeWorker(this);
        this.department = null;

        if(this.computer != null)
            this.computer.finishUsage();

        this.computer = null;
        this.card = null;
        this.directorate.updateInfo(this, this.workerName + " уволен");
    }

}
