package com.fisdom.worker;

public class Card {
    // Счётчик присвоенных id
    private static long curId = 0;

    // Поля класса
    private long cardNumber;

    /** Конструктор класса Card **/
    Card() {
        cardNumber = ++curId;
    }
}
