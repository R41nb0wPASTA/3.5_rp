package com.fisdom;

import com.fisdom.Exceptions.ComputerException;
import com.fisdom.worker.Worker;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Computer {
    // Поля класса
    private Worker worker;

    private long computerInventoryNumber;
    private boolean isBeingUsed = false;
    private LocalDateTime startOfUsage;
    private ArrayList<String> usage = new ArrayList<>();

    /** Конструктор класса Computer
     * @param computerInventoryNumber - инвентарный нормер компьютера
     * */
    public Computer(Long computerInventoryNumber){
        if(computerInventoryNumber < 0 || computerInventoryNumber == null){
            throw new ComputerException("Computers inventory number can't be less than zero or null");
        }
        this.computerInventoryNumber = computerInventoryNumber;
    }

    // Геттеры
    public Worker getWorker() {
        return worker;
    }

    public long getComputerInventoryNumber() {
        return computerInventoryNumber;
    }

    public boolean isBeingUsed() {
        return isBeingUsed;
    }

    public ArrayList<String> getUsage() {
        return usage;
    }

    // Методы класса Computer

    /** Начать использование компьютера
     * @param worker - работник, который будет работать за данным компьютером
     * @return boolean - успешность начала использования
     * */
    public boolean startUsage(Worker worker) {
        boolean isSet = worker != null && worker.setComputer(this);
        if(!isSet){
            throw new ComputerException("Worker can't be null!");
        }
        this.worker = worker;
        startOfUsage = LocalDateTime.now();
        isBeingUsed = true;

        return isSet;
    }

    /** Прекратить использование компьютера **/
    public void finishUsage() {
        if(!isBeingUsed || worker == null){
            throw new ComputerException("Can't stop usage if computer is not being used!");
        }
        usage.add(worker.getWorkerName() + " использовал компьютер " + computerInventoryNumber + " c "
                + startOfUsage.toString() + " по " + LocalDateTime.now().toString());
        worker = null;
        isBeingUsed = false;
    }

    /** Сформировать отчёт по работе компьютера
     * @return String - отчёт по работе компьютера
     * */
    public String createReport(){
        return String.join("\n", usage);
    }

}
